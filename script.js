let openMenu = document.querySelector('.main-icon .icon img')
let accordeon = document.querySelector('.accordeon')
let items = document.querySelectorAll('.accordeon .item')
// let div = document.createElement('div')

openMenu.addEventListener('click', () => {
    accordeon.classList.toggle('active');
    if (!accordeon.classList.contains('active')) {
        for (let item of items) {
            const content = item.querySelector('.row .icon img');
            item.classList.remove('active')
        }
    }
})

for (let item of items) {
    const mainContent = document.querySelector('.main-content')
    const iconForClick = item.querySelector('.row .icon');
    const titleForMainContent = item.querySelector('.row .title').innerText
    iconForClick.addEventListener('click', () => {
        if (accordeon.classList.contains('active')) {
            let wasActive = item.classList.contains('active')
            items.forEach((i) => {
                i.classList.remove('active')
            })
            if (!wasActive)
                item.classList.toggle('active');
            mainContent.innerText = titleForMainContent
            // для складніших структур
            // if (!mainContent.querySelector('div') === null)
            //     mainContent.removeChild(div)
            // div.innerText = titleForMainContent
            // mainContent.append(div)

        }
    })
}







